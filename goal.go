package patreon

type Goal struct {
	ID          string `jsonapi:"primary,goal"`
	Title       string `jsonapi:"attr,title"`
	AmountCents int64  `jsonapi:"attr,amount_cents"`
	Description string `jsonapi:"attr,description"`
}
