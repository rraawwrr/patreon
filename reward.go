package patreon

type Reward struct {
	ID          string `jsonapi:"primary,reward"`
	AmountCents int64  `jsonapi:"attr,amount_cents"`
	Description string `jsonapi:"attr,description"`
}
