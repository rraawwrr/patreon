package patreon

type Pledge struct {
	ID          string `jsonapi:"primary,pledge"`
	AmountCents int64  `jsonapi:"attr,amount_cents"`
	Creator     *User  `jsonapi:"relation,creator"`
	Patron      *User  `jsonapi:"relation,patron"`
}
