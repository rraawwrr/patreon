package patreon

type Campaign struct {
	ID      string    `jsonapi:"primary,campaign"`
	Goals   []*Goal   `jsonapi:"relation,goals"`
	Rewards []*Reward `jsonapi:"relation,rewwards"`
}
